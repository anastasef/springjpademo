package springJpaDemo.model;

import java.time.Instant;

import javax.persistence.*;

import org.hibernate.annotations.Type;

@Entity
@Table
public class Post {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//@NotBlank
	@Column(name = "title")
	private String title;
	
	@Lob
	@Type(type = "org.hibernate.type.TextType") // because the postgres' column is of type TEXT
	@Column(name = "content")
	//@NotEmpty
	private String content;
	
	@Column(name = "created_on")
	private Instant createdOn;
	
	@Column(name = "updated_on")
	private Instant updatedOn;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Instant getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Instant createdOn) {
		this.createdOn = createdOn;
	}

	public Instant getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Instant updatedOn) {
		this.updatedOn = updatedOn;
	}
}
