package springJpaDemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import springJpaDemo.model.Post;

public interface PostRepository extends JpaRepository<Post, Long>{

}
