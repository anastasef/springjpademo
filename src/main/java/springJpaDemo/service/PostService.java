package springJpaDemo.service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springJpaDemo.dto.PostDto;
import springJpaDemo.exception.PostNotFoundException;
import springJpaDemo.model.Post;
import springJpaDemo.repository.PostRepository;

@Service
public class PostService {
	
	@Autowired
	private PostRepository postRepository;

	public void createPost(PostDto postDto) {
		Post post = mapFromDtoToPost(postDto);
		postRepository.save(post);
	}
	
	public List<PostDto> showAllPosts() {
		List<Post> posts = postRepository.findAll();
		return posts.stream()
				.map(this::mapFromPostToDto)
				.collect(Collectors.toList());
	}
	
	public PostDto readSinglePost(Long id) {
		Post post = postRepository.findById(id)
				.orElseThrow(() -> new PostNotFoundException("Post with id = " + id + " not found"));
		return mapFromPostToDto(post);
	}
	
	// ---- Helper methods ----
	
	private PostDto mapFromPostToDto(Post post) {
		PostDto dto = new PostDto();
		dto.setId(post.getId());
		dto.setTitle(post.getTitle());
		dto.setContent(post.getContent());
		return dto;
	}
	
	private Post mapFromDtoToPost(PostDto dto) {
		Post post = new Post();
		post.setId(dto.getId());
		post.setTitle(dto.getTitle());
		post.setContent(dto.getContent());
		post.setCreatedOn(Instant.now()); // TODO вынести в createPost?
		return post;
	}
	
}
