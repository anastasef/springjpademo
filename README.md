# Spring JPA Demo

This project demonstrates some Spring JPA functionality.

### Technologies & tools used:

- Java SE 11
- Maven
- Spring boot
- Spring Data JPA
- PostgreSQL
- Flyway
- Hibernate
- HikariCP



## Links

- [Core JPA Annotations and Spring Data JPA](https://techrocking.com/spring-jpa-tutorial/)
- [Hibernate на русском](https://javastudy.ru/frameworks/hibernate/)
- [Hibernate tutorials](https://howtodoinjava.com/hibernate-tutorials/)