# Spring Boot Postgres tutorial

[video tutorial](https://www.youtube.com/watch?v=vtPkZShrvXQ)



Controller endpoint ----dto---> service method -----> repository method 



## **Initializer:**

- Spring Web Starter
- Spring Data JPA 
- PostgreSQL Driver



## Maven dependencies for the database:

```xml
<!-- api for db statements -->
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-jdbc</artifactId>
</dependency>

<!-- migrations -->
<dependency>
	<groupId>org.flywaydb</groupId>
	<artifactId>flyway-core</artifactId>
</dependency>

<!-- db driver -->
<dependency>
	<groupId>org.postgresql</groupId>
	<artifactId>postgresql</artifactId>
	<scope>runtime</scope>
</dependency>
```



## App packages

**Under main package create folders:**

- api (or controller)
- dao (repository)
- model
- service
- dto
- datasource - for db configuration
- exceptions



## Database

### Set up the docker container

**1** (postgres inside a docker container)

https://hub.docker.com/_/postgres

```lisp
[nast-pc ~]# docker run --name postgres-boot -e POSTGRES_PASSWORD=pass -d -p 5432:5432 postgres:alpine
```

```lisp
; The postgres container is running
[nast-pc ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
0d5a7f145e4a        postgres:alpine     "docker-entrypoint.s…"   23 hours ago        Up 2 seconds        0.0.0.0:5432->5432/tcp   postgres-boot
```

Start the container

```lisp
docker start 0d5a7f145e4a
```

Execute postgres commands inside the container

```lisp
; Execute bash inside the container
[nast-pc ~]# docker exec -it 0d5a7f145e4a bin/bash
bash-5.0# 

; Log in as a postgres default user
bash-5.0# psql -U postgres
psql (12.3)
Type "help" for help.

postgres=# 
```

Set up the database and the user

```lisp
; Create a new database
postgres=# CREATE DATABASE demodb;

; Create a new user
create user demodb_admin;

; Create a password for the user
alter user demodb_admin with encrypted password 'pass';

; Grant privileges to the user
grant all privileges on database demodb to demodb_admin;
```

Common commands

```lisp
; Connect as a user
psql -d demodb -U demodb_admin

; List everything
\d

; List tables
\dt

; Describe a table
\d table-name

; Quit
\q
```

List all databases

```lisp
postgres=# \l
                                 List of databases
   Name    |  Owner   | Encoding |  Collate   |   Ctype    |   Access privileges   
-----------+----------+----------+------------+------------+-----------------------
 demodb    | postgres | UTF8     | en_US.utf8 | en_US.utf8 | 
```

### Create db config

**2** Inside `datasource` package create a db config class `PostgresDatasource` with `@Configuration` annotation. Declare a bean `HikariDataSource` with annotation `@configurationProperties("app.datasource")`.

**3** Inside the `resources` package delete file `application.properties` and create a file `application.yml`.

**4** `application.properties`:

Create yml structure: 

```yml
app:
	datasource:
		# ... connection properties like username, pass etc
```

**5** Create a migration for flyway

1. Create directories: `myapp/src/main/resources/db/migrations`
2. Create a migration file: `V1__PostTable.sql` (we are going to create a `post` table)





## Model:

- define fields, getters and setters



## Dao (repository):

- create an interface for contract (available methods) for the model
- If using JDBC or have own custom methods -- create implementation class
  - add `@Repository("qualifierName")` annotation



## dto:

- create a dto class



## Service:

- create the entity service class - use dao interface methods
  - autowire dao impl class using the qualifier `@Qualifier("qualifierName")`
- methods
  - dto in params
  - call repository methods (JPA default or own implementations)
  - return dto



## api (or controller):

- create entity's controller class - use the service class
  - autowire the service class
- add annotation `@RestController` and `@RequestMapping("api/v1/something")`
- expose endpoints
  - add annotation to a method: `@PostMapping`
    - params `@RequestBody` dto
    - call service class methods, with dto as param
    - return `ResponseEntity`
  - `@GetMapping`
    - `@GetMapping("/endpoint/{id}")` path variable in method params `method(@PathVariable Long id)`



---

## Some notes

##### Postgres TEXT data type and Hibernate

If there is only `@Lob` annotation on the field, which in the db is of type TEXT, then it's value would be interpreted as a number. To solve this problem, add `@Type(type = "org.hibernate.type.TextType")` annotation to the field.

[link to the solution](https://shred.zone/cilla/page/299/string-lobs-on-postgresql-with-hibernate-36.html)

[related problem to this decision](https://discourse.hibernate.org/t/how-to-handle-lob-properties-when-using-2-different-databases-with-hibernate/2171/9)